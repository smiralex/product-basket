Инструкция по запуску сервера (нужна 11-я java):

    git clone https://gitlab.com/smiralex/product-basket.git
    cd product-basket
    mvn clean package
    java -jar target/product-basket-1.0-SNAPSHOT.jar
    
Пример тестирования обновления корзины:

    curl --header "Content-Type: application/json" \
    --request POST \
    --data  '{"products":[{"productName":"paper","numberOfProducts":3},{"productName":"agave","numberOfProducts":2}]}' \
    http://localhost:8080/basket/update
    
Пример тестирования добавления продуктов в корзину:

    curl --header "Content-Type: application/json" \
    --request POST \
    --data  '{"products":[{"productName":"paper","numberOfProducts":3},{"productName":"agave","numberOfProducts":2}]}' \
    http://localhost:8080/basket/add
    
Пример тестирования получения содержимого корзины

    curl -i --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --request GET \
    http://localhost:8080/basket/content
    
Примечание: не совсем понятно, как нужно было сделать обновление корзины: 
стирать все, что в ней было, или же добавлять к уже существующему.
Я решил сделать оба варианта.