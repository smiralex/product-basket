package ru.cft.shiftlab.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.cft.shiftlab.configuration.CurrentOrderConfig;
import ru.cft.shiftlab.entity.display.BasketContent;
import ru.cft.shiftlab.entity.display.Customer;
import ru.cft.shiftlab.entity.display.OrderProduct;
import ru.cft.shiftlab.entity.update.BasketUpdate;
import ru.cft.shiftlab.storage.ProductStorage;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/basket")
@ComponentScan(basePackages = {"ru.cft.shiftlab.configuration", "ru.cft.shiftlab.storage"})
public class RestApiController {

    private static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    private ProductStorage productStorage;

    @Autowired
    private CurrentOrderConfig orderConfig;

    @GetMapping("/content")
    public ResponseEntity<BasketContent> getBasketContent(){
        try{
            List<OrderProduct> orderProducts = productStorage.getAllProducts();
            Customer customer =
                    new Customer(orderConfig.getName(), orderConfig.getSurname(), orderConfig.getPatronymic());
            BasketContent basketContent =
                    new BasketContent(orderConfig.getOrderNumber(), customer, orderConfig.getDate(), orderProducts);
            logger.info("getBasketContent method invoke. Result: OK");
            return new ResponseEntity<>(basketContent, HttpStatus.OK);
        } catch (IOException e){
            logger.error("getBasketContent method invoke. Result: FAIL", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    public ResponseEntity<String> updateBasket(@RequestBody BasketUpdate basketUpdate) {
        try {
            productStorage.updateStorage(basketUpdate);
            logger.info("updateBasket method invoke with parameter {}. Result: OK", basketUpdate);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            logger.info("updateBasket method invoke with parameter {}. Result: {}", basketUpdate, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e){
            logger.info("updateBasket method invoke with parameter {}. Result: {}", basketUpdate, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            logger.error("updateBasket method invoke. Result: FAIL", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> addToBasket(@RequestBody BasketUpdate basketUpdate) {
        try {
            productStorage.addToStorage(basketUpdate);
            logger.info("addToBasket method invoke with parameter {}. Result: OK", basketUpdate);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            logger.info("addToBasket method invoke with parameter {}. Result: {}", basketUpdate, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e){
            logger.info("addToBasket method invoke with parameter {}. Result: {}", basketUpdate, e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            logger.error("addToBasket method invoke. Result: FAIL", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
