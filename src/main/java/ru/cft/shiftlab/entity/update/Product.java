package ru.cft.shiftlab.entity.update;

import java.io.Serializable;

public class Product implements Serializable{

    private String productName;
    private int numberOfProducts;

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public String getProductName() {
        return productName;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void increaseNumberOfProducts(int dif){
        numberOfProducts += dif;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + "(productName=" + productName + ", numberOfProducts=" + numberOfProducts + ")";
    }
}