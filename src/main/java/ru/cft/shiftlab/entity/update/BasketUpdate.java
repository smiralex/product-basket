package ru.cft.shiftlab.entity.update;

import java.io.Serializable;
import java.util.List;

public class BasketUpdate implements Serializable {

    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString(){
        return products.toString();
    }
}
