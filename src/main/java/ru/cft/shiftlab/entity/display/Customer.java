package ru.cft.shiftlab.entity.display;

import java.io.Serializable;

public class Customer implements Serializable {

    private String name;
    private String surname;
    private String patronymic;

    public Customer(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }
}
