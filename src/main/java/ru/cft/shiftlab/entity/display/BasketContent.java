package ru.cft.shiftlab.entity.display;

import java.io.Serializable;
import java.util.List;

public class BasketContent implements Serializable {

    private int orderNumber;
    private Customer customer;
    private String date;
    private List<OrderProduct> orderProducts;
    private double orderCost;

    public BasketContent(int orderNumber, Customer customer, String date, List<OrderProduct> orderProducts){
        this.orderNumber = orderNumber;
        this.customer = customer;
        this.date = date;
        this.orderProducts = orderProducts;
        this.orderCost = orderProducts == null ? 0 :
                 orderProducts.stream()
                .mapToDouble(OrderProduct::getTotalPrice)
                .sum();
    }

    public double getOrderCost() {
        return orderCost;
    }

    public List<OrderProduct> getOrderProducts() {
        return orderProducts;
    }

    public String getDate() {
        return date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public int getOrderNumber() {
        return orderNumber;
    }
}
