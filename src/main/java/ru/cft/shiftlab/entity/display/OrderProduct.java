package ru.cft.shiftlab.entity.display;

import java.io.Serializable;
import java.util.Map;

public class OrderProduct implements Serializable {

    private String productName;
    private int numberOfProducts;
    private Map<String, String> productParameters;
    private double totalPrice;

    public OrderProduct(String productName, int numberOfProducts,
                        double totalPrice, Map<String, String> productParameters){
        this.productName = productName;
        this.numberOfProducts = numberOfProducts;
        this.productParameters = productParameters;
        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public Map<String, String> getProductParameters() {
        return productParameters;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public String getProductName() {
        return productName;
    }
}
