package ru.cft.shiftlab.configuration;

import java.io.*;
import java.net.URL;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton class for loading product catalog
 */
public class CatalogConfig {

    private static final Logger logger = LoggerFactory.getLogger(CatalogConfig.class);
    private static final String CATALOG_FILE_NAME = "catalog.csv";
    private static final String PRODUCT_NAME_KEY = "productName";
    private static final String[] REQUIRED_HEADERS = {PRODUCT_NAME_KEY, "price"};
    private static CatalogConfig instance;

    private Map<String, Map<String, String>> catalog; // map productName to map with productParameters

    private CatalogConfig() throws IOException {
        InputStream inputStream = CatalogConfig.class.getClassLoader().getResourceAsStream(CATALOG_FILE_NAME);
        if (inputStream == null){
            logger.error("Failed to find catalog file {}", CATALOG_FILE_NAME);
            throw new IOException();
        }
        try (Reader in = new InputStreamReader(inputStream)){
            CSVParser parser = CSVParser.parse(in, CSVFormat.DEFAULT.withFirstRecordAsHeader());
            List<String> headers = parser.getHeaderNames();
            for (String header: REQUIRED_HEADERS){
                if (!headers.contains(header)) {
                    logger.error("Wrong format of catalog file {}", CATALOG_FILE_NAME);
                    throw new IOException();
                }
            }
            catalog = new HashMap<>();
            for (CSVRecord record : parser) {
                Map<String, String> parameters = new HashMap<>();
                catalog.put(record.get(PRODUCT_NAME_KEY), parameters);
                for (String header: headers){
                    if (!header.equals(PRODUCT_NAME_KEY)){
                        parameters.put(header, record.get(header));
                    }
                }
            }
        } catch (IOException e){
            logger.error("Failed to load information from catalog file {}", CATALOG_FILE_NAME);
            throw e;
        }
    }

    public static CatalogConfig getInstance() throws IOException{
        if (instance == null){
            instance = new CatalogConfig();
        }
        return instance;
    }

    public Map<String, Map<String, String>> getCatalog() {
        return catalog;
    }
}
