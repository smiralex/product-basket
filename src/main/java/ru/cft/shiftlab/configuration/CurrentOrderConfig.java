package ru.cft.shiftlab.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:current-order.properties")
@Scope("singleton") // by default
public class CurrentOrderConfig {

    @Value("${order-number}")
    private int orderNumber;

    @Value("${name}")
    private String name;

    @Value("${surname}")
    private String surname;

    @Value("${patronymic}")
    private String patronymic;

    @Value("${date}")
    private String date;

    public String getDate() {
        return date;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public int getOrderNumber() {
        return orderNumber;
    }
}
