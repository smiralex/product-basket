package ru.cft.shiftlab.storage;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import ru.cft.shiftlab.configuration.CatalogConfig;
import ru.cft.shiftlab.entity.display.OrderProduct;
import ru.cft.shiftlab.entity.update.BasketUpdate;
import ru.cft.shiftlab.entity.update.Product;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Component
public class ProductStorageJson implements ProductStorage{

    private static final Logger logger = LoggerFactory.getLogger(ProductStorageJson.class);
    private static final String STORAGE_DIR_NAME = "storage/";
    private static final String STORAGE_FILE_NAME= "products.json";

    private Path filePath;
    private Gson gson = new Gson();
    private Map<String, Map<String, String>> catalog;

    public ProductStorageJson() throws IOException{
        Path dirPath = Paths.get(STORAGE_DIR_NAME);
        if (!Files.exists(dirPath)){
            Files.createDirectory(dirPath);
        }
        filePath = Paths.get(STORAGE_DIR_NAME + STORAGE_FILE_NAME);
        if(!Files.exists(filePath)){
            Files.createFile(filePath);
        }
        catalog = CatalogConfig.getInstance().getCatalog();
    }

    @Override
    public void updateStorage(BasketUpdate basketUpdate) throws IOException {
        try {
            Map<String, Product> uniqueProducts = new HashMap<>();
            for (Product product : basketUpdate.getProducts()) {
                if (product.getNumberOfProducts() <= 0){
                    throw new NumberFormatException("Number of products " + product.getProductName() + " less than zero");
                }
                if (!catalog.containsKey(product.getProductName())) {
                    throw new NoSuchElementException("There are no product " + product.getProductName() + " in catalog");
                }
                Product uniqueProduct = uniqueProducts.get(product.getProductName());
                if (uniqueProduct == null) {
                    uniqueProducts.put(product.getProductName(), product);
                } else {
                    uniqueProduct.increaseNumberOfProducts(product.getNumberOfProducts());
                }
            }
            basketUpdate.setProducts(new ArrayList<>(uniqueProducts.values()));
            String json = gson.toJson(basketUpdate);
            Files.writeString(filePath, json);
        } catch (IOException e) {
            logger.error("Failed write to product storage file {}", filePath.toString());
            throw e;
        }
    }

    @Override
    public void addToStorage(BasketUpdate basketUpdate) throws IOException {
        try {
            String json = Files.readString(filePath);
            BasketUpdate basketUpdateOld = gson.fromJson(json, BasketUpdate.class);
            if (basketUpdateOld != null) {
                basketUpdate.getProducts().addAll(basketUpdateOld.getProducts());
            }
        }
        catch (IOException | JsonSyntaxException e) {
            logger.error("Failed to read from product storage file {}", filePath.toString());
            throw new IOException(e);
        }
        updateStorage(basketUpdate);
    }

    @Override
    public List<OrderProduct> getAllProducts() throws IOException {
        try {
            String json = Files.readString(filePath);
            BasketUpdate basketUpdate = gson.fromJson(json, BasketUpdate.class);
            if (basketUpdate == null){
                return null;
            }
            List<OrderProduct> orderProducts = new ArrayList<>();
            for (Product product : basketUpdate.getProducts()) {
                String productName = product.getProductName();
                int numberOfProducts = product.getNumberOfProducts();
                Map<String, String> productParameters = catalog.get(productName);
                String price = productParameters.get("price");
                try {
                    double totalPrice = numberOfProducts * Double.parseDouble(price);
                    OrderProduct orderProduct =
                            new OrderProduct(productName, numberOfProducts, totalPrice, productParameters);
                    orderProducts.add(orderProduct);
                } catch (NumberFormatException e) {
                    logger.error("Failed to parse price {} of product {} from catalog", price, productName);
                    throw new IOException();
                }
            }
            return orderProducts;
        } catch (IOException | JsonSyntaxException e) {
            logger.error("Failed to read from product storage file {}", filePath.toString());
            throw new IOException(e);
        }
    }
}
