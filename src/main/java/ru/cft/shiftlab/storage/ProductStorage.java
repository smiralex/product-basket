package ru.cft.shiftlab.storage;

import ru.cft.shiftlab.entity.display.OrderProduct;
import ru.cft.shiftlab.entity.update.BasketUpdate;

import java.io.IOException;
import java.util.List;

public interface ProductStorage {

    void updateStorage(BasketUpdate basketUpdate) throws IOException;

    void addToStorage(BasketUpdate basketUpdate) throws IOException;

    List<OrderProduct> getAllProducts() throws IOException;

}
